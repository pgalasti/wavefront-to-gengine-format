﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjToGEnginerConverter
{
    struct GModelOptions
    {
        public bool IncludeNormals;
        public bool IncludeUV;
    }
}
