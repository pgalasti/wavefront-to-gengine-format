﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ObjToGEnginerConverter.Structures;

namespace ObjToGEnginerConverter
{
    namespace IO
    {
        class GEngineFormatWriter
        {
            private GModelOptions options;

            public GEngineFormatWriter(GModelOptions options)
            {
                this.options = options;
            }

            public GModelOptions Options
            {
                get { return this.options; }
                set { this.options = value; }

            }

            public void WriteToFile(List<TriangleLine> triangles, List<IndexLine> indicies, String filePath)
            {
                System.IO.StreamWriter file = new System.IO.StreamWriter(filePath, false);

                this.WriteHeader(file, triangles, indicies);

                foreach (TriangleLine line in triangles)
                {
                    String strLine = String.Format("V|{0}|{1}|{2}|",
                    line.VertexLine.Verticies[0], line.VertexLine.Verticies[1], line.VertexLine.Verticies[2]);

                    if (this.options.IncludeNormals)
                    {
                        strLine += String.Format("{0}|{1}|{2}|",
                        line.NormalLine.VertexNormals[0], line.NormalLine.VertexNormals[1], line.NormalLine.VertexNormals[2]);
                    }

                    if (this.options.IncludeUV)
                    {
                        strLine += String.Format("{0}|{1}",
                            line.UvLine.UVMapping[0], line.UvLine.UVMapping[1]);
                    }

                    file.WriteLine(strLine);
                }

                for (int i = 1; i < indicies.Count; i+=3)
                {
                    
                    String strLine = String.Format("i|{0}|{1}|{2}|",
                        i, i+1, i+2);
                    file.WriteLine(strLine);
                }

                file.Close();
            }

            private void WriteHeader(System.IO.StreamWriter file, List<TriangleLine> triangles, List<IndexLine> indicies)
            {
                // Write header with version
                file.WriteLine("## GMODEL:v1.0 ==========");

                // Write features
                file.Write("## V-I"); // Always indicate verticies and indicies with gmodel files.
                if (this.options.IncludeNormals)
                {
                    file.Write("-N");
                }
                if (this.options.IncludeUV)
                {
                    file.Write("-T");
                }

                file.WriteLine();

                // Write Statistics
                file.WriteLine(String.Format("$ VertexCount:{0}", triangles.Count));
                file.WriteLine(String.Format("$ IndexCount:{0}", indicies.Count));
            }
        }
    }
}
