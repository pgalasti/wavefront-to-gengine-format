﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ObjToGEnginerConverter.Structures;

namespace ObjToGEnginerConverter.IO
{
    class WavefrontFormatReader
    {
        public WavefrontFormatReader()
        {

        }

        
        public static List<VertexLine> ReadVerticies(String filePath)
        {
            List<VertexLine> vertexLineList = new List<VertexLine>();

            System.IO.StreamReader fileReader = new System.IO.StreamReader(filePath);

            String line;
            while((line = fileReader.ReadLine()) != null)
            {
                if (line[0] != 'v' && line[0] != 'V')
                    continue;

                // Make sure we're not looking at the vertex normal section
                if (line[1] != ' ')
                    continue;

                VertexLine vertexLine = new VertexLine();
                String[] tokens = line.Split(' ');

                // Take just the first 3 verticies
                vertexLine.Verticies[0] = (float)Convert.ToDouble(tokens[1]);
                vertexLine.Verticies[1] = (float)Convert.ToDouble(tokens[2]);
                vertexLine.Verticies[2] = (float)Convert.ToDouble(tokens[3]);

                vertexLineList.Add(vertexLine);
            }

            return vertexLineList;
        }

        public static List<NormalsLine> ReadVertexNormals(String filePath)
        {
            List<NormalsLine> normalsLineList = new List<NormalsLine>();

            System.IO.StreamReader fileReader = new System.IO.StreamReader(filePath);

            String line;
            while((line = fileReader.ReadLine()) != null)
            {
                if (line[0] != 'v' && line[0] != 'V')
                    continue;

                // Make sure we're not looking at the vertex normal section
                if (line[1] != 'n' && line[1] != 'N')
                    continue;

                NormalsLine normalsLine = new NormalsLine();
                String[] tokens = line.Split(' ');

                // Take just the first 3 verticies
                normalsLine.VertexNormals[0] = (float)Convert.ToDouble(tokens[1]);
                normalsLine.VertexNormals[1] = (float)Convert.ToDouble(tokens[2]);
                normalsLine.VertexNormals[2] = (float)Convert.ToDouble(tokens[3]);

                normalsLineList.Add(normalsLine);
            }

            return normalsLineList;
        }

        public static List<UVLine> ReadUVMapping(String filePath)
        {
            List<UVLine> uvLineList = new List<UVLine>();

            System.IO.StreamReader fileReader = new System.IO.StreamReader(filePath);

            String line;
            while ((line = fileReader.ReadLine()) != null)
            {
                if (line[0] != 'v' && line[0] != 'V')
                    continue;

                if (line[1] != 't' && line[1] != 'T')
                    continue;

                UVLine uvLine = new UVLine();
                String[] tokens = line.Split(' ');

                uvLine.UVMapping[0] = (float)Convert.ToDouble(tokens[1]);
                uvLine.UVMapping[1] = (float)Convert.ToDouble(tokens[2]);

                uvLineList.Add(uvLine);
            }

            return uvLineList;
        }

        public static List<IndexLine> ReadIndicies(String filePath)
        {
            List<IndexLine> IndexLineList = new List<IndexLine>();

            System.IO.StreamReader fileReader = new System.IO.StreamReader(filePath);

            String line;
            while ((line = fileReader.ReadLine()) != null)
            {
                if (line[0] != 'f' && line[0] != 'F')
                    continue;

                String[] tokens = line.Split(' ');

                for (int i = 0; i < 3; i++ )
                {
                    IndexLine indiciesLine = new IndexLine();
                    indiciesLine.VertexIndex = Convert.ToUInt32(tokens[i + 1].Split('/')[0]);
                    indiciesLine.UVIndex = Convert.ToUInt32(tokens[i + 1].Split('/')[1]);
                    indiciesLine.NormalIndex = Convert.ToUInt32(tokens[i + 1].Split('/')[2]);
                    IndexLineList.Add(indiciesLine);
                }
            }

            return IndexLineList;
        }

        public static Dictionary<uint, uint> ReaderVertexToNormalMap(String filePath)
        {
            Dictionary<uint, uint> vertexToNormalMap = new Dictionary<uint, uint>();

            System.IO.StreamReader fileReader = new System.IO.StreamReader(filePath);

            String line;
            while ((line = fileReader.ReadLine()) != null)
            {
                if (line[0] != 'f' && line[0] != 'F')
                    continue;

                String[] tokens = line.Split(' ');

                for (int i = 0; i < 3; i++)
                {
                    String[] stringValues = tokens[i + 1].Split('/');
                    uint vertexIndex = Convert.ToUInt32(stringValues[0]);
                    uint normalIndex = Convert.ToUInt32(stringValues[2]);
                    if(!vertexToNormalMap.ContainsKey(vertexIndex))
                        vertexToNormalMap.Add(vertexIndex, normalIndex);
                }
            }

            return vertexToNormalMap;
        }
    }
}
