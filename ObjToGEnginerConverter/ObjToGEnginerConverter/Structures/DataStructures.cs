﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjToGEnginerConverter
{
    namespace Structures
    {
        public class TriangleLine
        {
            public VertexLine VertexLine = new VertexLine();
            public NormalsLine NormalLine = new NormalsLine();
            public UVLine UvLine = new UVLine();
        }

        public class VertexLine
        {
            public float[] Verticies = new float[3];
        }

        public class NormalsLine
        {
            public float[] VertexNormals = new float[3];
        }

        public class UVLine
        {
            public float[] UVMapping = new float[2];
        }

        public class IndexLine
        {
            public uint VertexIndex;
            public uint NormalIndex;
            public uint UVIndex;
        }
    }
}
