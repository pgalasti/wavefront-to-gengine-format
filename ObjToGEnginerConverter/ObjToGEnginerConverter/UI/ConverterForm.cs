﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ObjToGEnginerConverter.Structures;
using ObjToGEnginerConverter.IO;


namespace ObjToGEnginerConverter
{
    namespace UI
    {
        public partial class ConverterForm : Form
        {
            public ConverterForm()
            {
                InitializeComponent();
            }

            private void btnConvert_Click(object sender, EventArgs e)
            {
                String objFilePath = txbWavefrontPath.Text;
                List<VertexLine> vertexLines = WavefrontFormatReader.ReadVerticies(objFilePath);
                List<NormalsLine> normalsLines = WavefrontFormatReader.ReadVertexNormals(objFilePath);
                List<UVLine> uvLines = WavefrontFormatReader.ReadUVMapping(objFilePath);
                List<IndexLine> indexLines = WavefrontFormatReader.ReadIndicies(objFilePath);
                Dictionary<uint, uint> vertexToNormalMap = WavefrontFormatReader.ReaderVertexToNormalMap(objFilePath);

                List<TriangleLine> triangles = new List<TriangleLine>();
                foreach (IndexLine indexLine in indexLines)
                {
                    TriangleLine triangleLine = new TriangleLine();
                    triangleLine.VertexLine = vertexLines[(int)indexLine.VertexIndex - 1];
                    triangleLine.NormalLine = normalsLines[(int)indexLine.NormalIndex - 1];
                    triangleLine.UvLine = uvLines[(int)indexLine.UVIndex - 1];
                    triangles.Add(triangleLine);
                }

                GModelOptions options = new GModelOptions();
                options.IncludeNormals = this.cbxIncludeNormals.Checked;
                options.IncludeUV = this.cbxIncludeUVMap.Checked;
                GEngineFormatWriter writer = new GEngineFormatWriter(options);
                writer.WriteToFile(triangles, indexLines, txbGEnginePath.Text);
            }
        }
    }
}