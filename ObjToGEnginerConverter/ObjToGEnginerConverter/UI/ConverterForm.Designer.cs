﻿namespace ObjToGEnginerConverter
{
    namespace UI
    {

        partial class ConverterForm
        {
            /// <summary>
            /// Required designer variable.
            /// </summary>
            private System.ComponentModel.IContainer components = null;

            /// <summary>
            /// Clean up any resources being used.
            /// </summary>
            /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
            protected override void Dispose(bool disposing)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }

            #region Windows Form Designer generated code

            /// <summary>
            /// Required method for Designer support - do not modify
            /// the contents of this method with the code editor.
            /// </summary>
            private void InitializeComponent()
            {
            this.txbWavefrontPath = new System.Windows.Forms.TextBox();
            this.txbGEnginePath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbxIncludeNormals = new System.Windows.Forms.CheckBox();
            this.cbxIncludeUVMap = new System.Windows.Forms.CheckBox();
            this.btnConvert = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txbWavefrontPath
            // 
            this.txbWavefrontPath.Location = new System.Drawing.Point(12, 36);
            this.txbWavefrontPath.Name = "txbWavefrontPath";
            this.txbWavefrontPath.Size = new System.Drawing.Size(272, 20);
            this.txbWavefrontPath.TabIndex = 0;
            this.txbWavefrontPath.Text = "C:\\Users\\Paul\\Documents\\CubeUV.obj";
            // 
            // txbGEnginePath
            // 
            this.txbGEnginePath.Location = new System.Drawing.Point(12, 84);
            this.txbGEnginePath.Name = "txbGEnginePath";
            this.txbGEnginePath.Size = new System.Drawing.Size(272, 20);
            this.txbGEnginePath.TabIndex = 1;
            this.txbGEnginePath.Text = "C:\\Users\\Paul\\Documents\\CubeUV.gmodel";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Wavefront Object File";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "GEngine Object File";
            // 
            // cbxIncludeNormals
            // 
            this.cbxIncludeNormals.AutoSize = true;
            this.cbxIncludeNormals.Location = new System.Drawing.Point(326, 36);
            this.cbxIncludeNormals.Name = "cbxIncludeNormals";
            this.cbxIncludeNormals.Size = new System.Drawing.Size(135, 17);
            this.cbxIncludeNormals.TabIndex = 4;
            this.cbxIncludeNormals.Text = "Include Vertex Normals";
            this.cbxIncludeNormals.UseVisualStyleBackColor = true;
            // 
            // cbxIncludeUVMap
            // 
            this.cbxIncludeUVMap.AutoSize = true;
            this.cbxIncludeUVMap.Location = new System.Drawing.Point(326, 64);
            this.cbxIncludeUVMap.Name = "cbxIncludeUVMap";
            this.cbxIncludeUVMap.Size = new System.Drawing.Size(103, 17);
            this.cbxIncludeUVMap.TabIndex = 5;
            this.cbxIncludeUVMap.Text = "Include UV Map";
            this.cbxIncludeUVMap.UseVisualStyleBackColor = true;
            // 
            // btnConvert
            // 
            this.btnConvert.Location = new System.Drawing.Point(387, 203);
            this.btnConvert.Name = "btnConvert";
            this.btnConvert.Size = new System.Drawing.Size(75, 23);
            this.btnConvert.TabIndex = 6;
            this.btnConvert.Text = "Convert";
            this.btnConvert.UseVisualStyleBackColor = true;
            this.btnConvert.Click += new System.EventHandler(this.btnConvert_Click);
            // 
            // ConverterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 238);
            this.Controls.Add(this.btnConvert);
            this.Controls.Add(this.cbxIncludeUVMap);
            this.Controls.Add(this.cbxIncludeNormals);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txbGEnginePath);
            this.Controls.Add(this.txbWavefrontPath);
            this.Name = "ConverterForm";
            this.Text = "Wavefront To GEngine Format Converter";
            this.ResumeLayout(false);
            this.PerformLayout();

            }

            #endregion

            private System.Windows.Forms.TextBox txbWavefrontPath;
            private System.Windows.Forms.TextBox txbGEnginePath;
            private System.Windows.Forms.Label label1;
            private System.Windows.Forms.Label label2;
            private System.Windows.Forms.CheckBox cbxIncludeNormals;
            private System.Windows.Forms.CheckBox cbxIncludeUVMap;
            private System.Windows.Forms.Button btnConvert;
        }
    }

}